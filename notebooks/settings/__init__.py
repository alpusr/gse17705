import os

DATA_FOLDER = '../data/'

# Raw data
RAW_FOLDER = os.path.join(DATA_FOLDER, 'raw')
SERIES_FILE = os.path.join(RAW_FOLDER, 'GSE17705_series_matrix.txt')

# Processed data
DF_FOLDER = os.path.join(DATA_FOLDER, 'df')
RAW_DF_FILE = os.path.join(DF_FOLDER, 'raw.df')

EXPRESSIONS_DF_FILE = os.path.join(DF_FOLDER, 'expressions.df')
EXPRESSIONS_STAND_DF_FILE = os.path.join(DF_FOLDER, 'expressions_stand.df')
TARGETS_DF_FILE = os.path.join(DF_FOLDER, 'targets.df')

MRMR_50_LIST = os.path.join(DF_FOLDER, 'mrmr50.csv')
MRMR_100_LIST = os.path.join(DF_FOLDER, 'mrmr100.csv')
MRMR_50_STAND_LIST = os.path.join(DF_FOLDER, 'mrmr50_stand.csv')
MRMR_100_STAND_LIST = os.path.join(DF_FOLDER, 'mrmr100_stand.csv')

