install.packages("mRMRe")
library(mRMRe)

mRMRe::set.thread.count(8)

df <- read.csv('../data/df/expressions.df', colClasses = 'numeric')
targets <- read.csv('../data/df/targets.df', colClasses = 'numeric')
targets <- targets$distant_relapse

concat <- data.frame(df, targets)
dd <- mRMR.data(data = concat)

targets_num <- which(colnames(concat)=="targets")
res50 <- mRMR.classic(data = dd, target_indices = targets_num, feature_count = 50)
res100 <- mRMR.classic(data = dd, target_indices = targets_num, feature_count = 100)

write.csv(res50@filters, file = '../data/df/res50.csv')
write.csv(res100@filters, file = '../data/df/res100.csv')