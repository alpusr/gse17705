# GSE17705

Для воссоздания решения:

* Установить все python3 зависимости из requirements.txt
* В папку `data/raw` нужно поместить все файлы из списка `data/raw_files_list.txt`, скачав их с https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=gse17705 .
* Для обработки сырых данных использовать `notebooks/raw_dataset.ipynb`
* Для преобразования в удобынй формат использовать `notebooks/data_transformation.ipynb`
* "Глупое" решение `notebooks/baseline_random.ipynb`
* Нахождение фич алгоритмом MRMR с помощью скрипта `notebooks/count_mrmr.r`
* Нахождение фич алгоритмом Inf-FS с помощью скрипта `notebooks/ifs.ipynb`
* Нахождение фич алгоритмом mIFS с помощью скрипта `notebooks/mifs.ipynb`
* Нахождение фич алгоритмом SIFS с помощью скрипта `notebooks/SIFS.ipynb`
* Применение SVM для всех отобранных признаков `notebooks/svm.ipynb`

Данные также находятся в репозитории, так что можно использовать любой скрипт без выполнения предыдущих.